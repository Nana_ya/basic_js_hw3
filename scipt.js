let num = Number(prompt("Input your number, please!"));
let notNumber = "It's not a number.";
let notInt = "It's not integer. Please, input integer!";
let negative = "The number is negative.";
let noNumbers = "Sorry, no numbers";
let tryAgain = "Please, try again.";
let inputM = "Input m number, please! (all numbers must be greater than 1)";
let inputN = "Input n number, please! (n should be greater than m & all numbers must be greater than 1)";
let m, n;

console.log("1. Mandatory task:");
multipleOfFive(num);

console.log("///////////////////");

console.log("2. Optional task:")
m = Number(prompt(inputM));
checkm(m);
n = Number(prompt(inputN));
checkn(n, m);

let empty = 0;
nextStep: for (let i = m; i <= n; i++) {
	//4 7
	
	for (let j = 2; j <= i; j++) {
		if (i % j == 0 && i != j) {
			continue nextStep;
		}
	}
	console.log(i);
	empty += i;
}
if (empty == 0){console.log(`No prime numbers between ${m} and ${n}`)};




function multipleOfFive(checkNum) {
	while (isNaN(checkNum) || checkNum == 0 || checkNum % 1 !== 0 || checkNum < 5) {
		if (isNaN(checkNum)) {
			checkNum = prompt(notNumber, "");
		} else if (checkNum == 0) {
			checkNum = prompt(noNumbers + ".", "");
		} else if (checkNum % 1 !== 0) {
			checkNum = prompt(notInt, "");
		} else if(checkNum < 5){
			checkNum = prompt(noNumbers + " from 0 to " + `${checkNum}`);
		}
	}
	
	for (let i = 0; i <= checkNum; i++) {
		if (i % 5 == 0 && i !== 0) {
			console.log(i);
		}
	}
}

function checkm(checkM) {
	while (isNaN(checkM) || checkM == 0 || checkM % 1 !== 0 || checkM <= 1) {
		checkM = Number(prompt(`${tryAgain} ${inputM}`));
	}
}

function checkn(checkN, checkM) {
	while (isNaN(checkN) || checkN % 1 !== 0 || checkN <= 1 || checkN <= checkM) {
		checkM = Number(prompt(`${tryAgain} ${inputM}`));
		checkm(checkM);
		checkN = Number(prompt(`${inputN}`));
	}
}
